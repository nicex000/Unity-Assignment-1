using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Stage
{
    BIG,
    MEDIUM,
    SMALL
}


public class Breakup : MonoBehaviour
{

    [SerializeField] private GameObject MediumBall;
    [SerializeField] private GameObject SmallBall;
    public Stage stage = Stage.BIG;

    [Header("Settings")]
    [SerializeField] private float InvulnerabilityTime;
    [SerializeField] private float SeparationTime;
    private float invulnTimer;
    [SerializeField] private float StationaryTime;
    private float stationaryDeleteTimer;
    [SerializeField] private float MinLivingSpeed;
    [SerializeField] private bool UseMassBasedSplit;
    [SerializeField] private int SplitCount;
    [SerializeField] private float SplitAngle;

    private bool isCollidingAlready;
    public bool IsCollidingAlready { set { isCollidingAlready = true; } }
    private new Rigidbody rigidbody;
    private List<GameObject> invulnOthers;
    public List<GameObject> InvulnOthers { set { invulnOthers = value; }}


    // Start is called before the first frame update
    void Start()
    {
        invulnTimer = 0f;
        stationaryDeleteTimer = 0f;
        isCollidingAlready = false;
        rigidbody = this.GetComponent<Rigidbody>();
        if (invulnOthers == null) invulnOthers = new List<GameObject>();
        if (SeparationTime > 0f) StartCoroutine(SpawnTriggerCoroutine());
    }

    // Update is called once per frame
    void Update()
    {
        if (invulnTimer < InvulnerabilityTime)
        {
            invulnTimer += Time.deltaTime;
        }

        if (rigidbody && rigidbody.velocity.magnitude < MinLivingSpeed && StationaryTime > 0f)
        {
            if (stationaryDeleteTimer > StationaryTime)
            {
                Reduce(null);
            }
            stationaryDeleteTimer += Time.deltaTime;
        }
        else if (stationaryDeleteTimer > 0f)
        {
            stationaryDeleteTimer = 0f;
        }
    }

    private IEnumerator SpawnTriggerCoroutine()
    {
        foreach (var collider in transform.GetComponentsInChildren<Collider>())
        {
            collider.isTrigger = true;
        }
        yield return new WaitForSeconds(SeparationTime);
        foreach (var collider in transform.GetComponentsInChildren<Collider>())
        {
            collider.isTrigger = false;
        }
    }

    public List<GameObject> DestroyByOther(GameObject triggeringObject, List<GameObject> others)
    {
        if (invulnTimer >= InvulnerabilityTime ||
             invulnOthers.FindIndex(obj => obj.GetInstanceID() == triggeringObject.GetInstanceID()) == -1)
        {
            return Reduce(others);
        }
        return new List<GameObject> { this.gameObject };
    }

    public void AddInvulnOthers(List<GameObject> others)
    {
        if (others != null) invulnOthers.AddRange(others);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor")) return;

        var otherRoot = collision.gameObject.transform.root.gameObject;
        if ((invulnTimer >= InvulnerabilityTime ||
            invulnOthers.FindIndex(obj =>
                obj.GetInstanceID() == collision.gameObject.transform.root.gameObject.GetInstanceID()
            ) == -1) && 
            !isCollidingAlready)
        {
            var otherBreakup = otherRoot.GetComponent<Breakup>();
            if (otherBreakup != null)
            {
                otherBreakup.IsCollidingAlready = true;
                var children = Reduce(null);
                var others = otherBreakup.DestroyByOther(this.gameObject, children);

                if (children != null)
                    foreach (var obj in children)
                    {
                        obj.GetComponent<Breakup>().AddInvulnOthers(others);
                    }

            }
            else
            {
                Reduce(null);
            }
        }
    }


    private List<GameObject> Reduce(List<GameObject> others)
    {
        isCollidingAlready = true;
        List<GameObject> children = null;
        switch (stage)
        {
            case Stage.BIG:
                children = SpawnSmaller(stage, others);
                break;
            case Stage.MEDIUM:
                children = SpawnSmaller(stage, others);
                break;
            case Stage.SMALL:
                break;
        }
        Destroy(this.gameObject);
        return children;
    }


    private List<GameObject> SpawnSmaller(Stage stage, List<GameObject> others)
    {
        GameObject objToSpawn = null;
        stage++;
        if (stage == Stage.MEDIUM)
        {
            objToSpawn = MediumBall;
        }
        else
        {
            objToSpawn = SmallBall;
        }

        Vector3 headingDirection = rigidbody.velocity;
        transform.LookAt(transform.position + headingDirection.normalized);
        var children = new List<GameObject>();
        if (UseMassBasedSplit)
        {
            SplitCount = (int)(rigidbody.mass / objToSpawn.GetComponent<Rigidbody>().mass);
        }
        for (int i = 0; i < SplitCount; ++i)
        {
            transform.Rotate(Vector3.forward * Random.Range(90, 270));
            var obj = Instantiate(objToSpawn, transform.position, transform.rotation);
            obj.transform.Rotate(Vector3.up * SplitAngle);
            obj.GetComponent<Rigidbody>().velocity = obj.transform.forward * headingDirection.magnitude;
            obj.GetComponent<Breakup>().stage = stage;
            if (UseMassBasedSplit) obj.GetComponent<Breakup>().UseMassBasedSplit = UseMassBasedSplit;
            children.Add(obj);
        }
        var invulns = new List<GameObject>();
        if (others != null)
        {
            invulns.AddRange(others);
        }
        invulns.AddRange(children);
        foreach (var obj in children) obj.GetComponent<Breakup>().InvulnOthers = invulns;
        return children;
    }

}
