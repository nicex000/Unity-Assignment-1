using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasRotation : MonoBehaviour
{
    [SerializeField] private Canvas canvas;
    [SerializeField] private Button button;
    [SerializeField] private Slider slider;
    [SerializeField] private Text text;
    [SerializeField] private Sprite onBtn;
    [SerializeField] private Sprite offBtn;
    private bool shootingState;
    private Turret turretScript;

    // Start is called before the first frame update
    void Start()
    {
        canvas.worldCamera = Camera.main;
        turretScript = GetComponentInParent<Turret>();
        transform.SetParent(transform.parent.parent, false);
        shootingState = false;
        ToggleShooting();
        SetSliderText(slider.value);
    }

    // Update is called once per frame
    void Update()
    {
        if (canvas.worldCamera != null)
        {
            var dir = canvas.worldCamera.transform.position - transform.position;
            var cardinal = Mathf.Abs(dir.x) > Mathf.Abs(dir.z) ? Vector3.right * Mathf.Sign(dir.x) : Vector3.forward * Mathf.Sign(dir.z);

            canvas.transform.LookAt(canvas.transform.position + cardinal);
        }
        
    }

    public void ToggleShooting()
    {
        shootingState = !shootingState;
        if (turretScript != null) turretScript.SpawningEnabled = shootingState;
        if (shootingState)
        {
            ((Image)button.targetGraphic).sprite = onBtn;
        }
        else
        {
            ((Image)button.targetGraphic).sprite = offBtn;
        }
    }
    public void SetSliderText(float value)
    {
        text.text = value.ToString("0.0");
    }
}
