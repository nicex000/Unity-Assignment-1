using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : MonoBehaviour
{
    [SerializeField] private Transform[] SpawnPoints = new Transform[4];
    [SerializeField] private GameObject SpawnerPrefab;
    [SerializeField] private GameObject BallPrefab;

    [Header("Settings")]
    [SerializeField] private Vector2 MaxAngle;
    [SerializeField] private float MinForce;
    [SerializeField] private float MaxForce;
    [SerializeField] private float BallSpawnTime;
    [SerializeField] private float RotationSeekTime;

    private void Awake()
    {
        foreach (var spawnPoint in SpawnPoints)
        {
            var turret = Instantiate(SpawnerPrefab, spawnPoint).GetComponent<Turret>();
            turret.InitializeValues(BallPrefab, MaxAngle, RotationSeekTime, BallSpawnTime, MinForce, MaxForce);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }
}
