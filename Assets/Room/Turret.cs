using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    [SerializeField]
    private Transform SpawnPoint;

    [SerializeField][Range(0.5f, 2f)]
    private float spawnSpeed = 0.5f;
    public float SpawnSpeed { set{ spawnSpeed = value; } }
    private float spawnCooldown;

    private bool spawningEnabled;
    public bool SpawningEnabled { set{ spawningEnabled = value; } }

    private float nextRotationTimer;
    private Quaternion targetRotation;
    private Quaternion originalRotation;
    private Quaternion startRotation;


    private GameObject ballPrefab;
    private Vector2 maxAngle;
    private float rotationSeekTime;
    private float spawnTimer;
    private float minForce;
    private float maxForce;


    // Start is called before the first frame update
    void Start()
    {
        nextRotationTimer = Random.Range(0f, rotationSeekTime);
        originalRotation = this.transform.rotation;
        targetRotation = originalRotation;
        spawnCooldown = Random.Range(0f, spawnTimer);
        spawningEnabled = true;
    }

    public void InitializeValues(GameObject ballPrefab,
                          Vector2 maxAngle,
                          float rotationSeekTime,
                          float ballSpawnTime,
                          float minForce,
                          float maxForce)
    {
        this.ballPrefab = ballPrefab;
        this.maxAngle = maxAngle;
        this.rotationSeekTime = rotationSeekTime;
        spawnTimer = ballSpawnTime;
        this.minForce = minForce;
        this.maxForce = maxForce;
    }

    // Update is called once per frame
    void Update()
    {
        if (spawningEnabled)
        {
            while (spawnCooldown >= spawnTimer)
            {
                spawnCooldown -= spawnTimer;
                SpawnBall(Random.Range(minForce, maxForce));
            }
            spawnCooldown += Time.deltaTime * spawnSpeed;
        }


        if (this.transform.rotation != targetRotation)
        {
            this.transform.rotation = Quaternion.Slerp(startRotation, targetRotation, nextRotationTimer / rotationSeekTime);
        }


        if (nextRotationTimer >= rotationSeekTime)
        {
            nextRotationTimer = 0f;
            SetNewRotation();
        }
        else
        {
            nextRotationTimer += Time.deltaTime;
        }
    }

    public void ToggleSpawning()
    {

    }


    private void SetNewRotation()
    {
        startRotation = this.transform.rotation;
        targetRotation = originalRotation * Quaternion.Euler(
            Random.Range(-maxAngle.y, maxAngle.y),
            Random.Range(-maxAngle.x, maxAngle.x),
            0);
    }

    public void SpawnBall(float force)
    {
        if (SpawnPoint != null)
        {
            var ball = Instantiate(ballPrefab, SpawnPoint.position, SpawnPoint.rotation);
            ball.GetComponent<Rigidbody>().AddForce(
                ball.transform.forward * force, ForceMode.Impulse);
        }
    }
}
