# Unity Assignment 1

This is a school assignment for the Mobile class, it's just a simple Unity project.

Balls spawn randomly from turrets. When they interact with another ball or a wall they get destroyed and smaller balls spawn in their place. Small sized balls simply get destroyed.
